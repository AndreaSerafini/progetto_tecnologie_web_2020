#!/bin/sh

ssh -o StrictHostKeyChecking=no ubuntu@$IP_DEPLOY << 'ENDSSH'
  
  cd ~
  export $(cat .env | xargs)
  echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  docker pull $IMAGE:web
  docker pull $IMAGE:nginx
  docker-compose -f docker-compose.prod.yml up -d 

ENDSSH

