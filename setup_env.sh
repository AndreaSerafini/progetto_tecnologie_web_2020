#! /bin/bash

echo DEBUG=0 >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env

echo SECRET_KEY=$SECRET_KEY >> .env

echo DJANGO_ALLOWED_HOSTS=$IP_DEPLOY >> .env

echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=db >> .env
echo SQL_PORT=5432 >> .env

echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env

echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_REGISTRY_PASSWORD=$CI_REGISTRY_PASSWORD  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env

echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env

echo DJANGO_SUPERUSER_PASSWORD=$DJANGO_SUPERUSER_PASSWORD >> .env
echo ADMIN_EMAIL=$ADMIN_EMAIL >> .env
echo ADMIN_USERNAME=$ADMIN_USERNAME >> .env

echo POSTGRES_USER=$POSTGRES_USER >> .db.env
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >> .db.env
echo POSTGRES_DB=$POSTGRES_DB >> .db.env

