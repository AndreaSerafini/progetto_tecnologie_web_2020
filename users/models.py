from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    # https://code.djangoproject.com/ticket/31725#no1
    avatar = models.ImageField(upload_to='avatars/', null=False, blank=False, default="default.png")
    bio = models.TextField(verbose_name="Descrizione")
    first_name = models.CharField(('first name'), max_length=30, blank=False)
    last_name = models.CharField(('last name'), max_length=150, blank=False)

    def get_full_name(self):
        return str(self.first_name + " " + self.last_name).strip()

    def get_avatar(self):
        return self.avatar.file

    def get_favourite_composers(self):
        return self.favourite_composers.all()

    def get_favourite_genres(self):
        return self.favourite_genres.all()
