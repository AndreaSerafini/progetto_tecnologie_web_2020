from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect

from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import CreateView

from users.forms import UserCrispyForm, UserEditForm
from users.models import User

account_activation_token = PasswordResetTokenGenerator()


class UserCreateView(CreateView):
    form_class = UserCrispyForm
    template_name = 'users/register.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super(UserCreateView, self).form_valid(form)

        mail_subject = 'Sound Share: confrema indirizzo email'
        relative_confirm_url = reverse(
            'users:verify-user-email',
            args=[
                urlsafe_base64_encode(force_bytes(self.object.pk)),
                account_activation_token.make_token(self.object)
            ]
        )

        self.object.email_user(
            subject=mail_subject,
            message=f'''Ciao {self.object.username},
            e benvenuto sul nostro sito!

            Clicca il link seguente per confermare il tuo account:
            {self.request.build_absolute_uri(relative_confirm_url)}

            Sound Share'''
        )

        self.object.token_sent = True
        self.object.is_active = False
        self.object.save()

        return response


def user_login_by_token(request, user_id_b64=None, user_token=None):
    try:
        uid = force_text(urlsafe_base64_decode(user_id_b64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, user_token):
        user.is_active = True
        user.save()

        return True

    return False


def verify_user_email(request, user_id_b64=None, user_token=None):
    if not user_login_by_token(request, user_id_b64, user_token):
        message = 'Errore: Tentativo non valido di validazione email'
    else:
        message = 'Email confermata correttamente!'
    return render(request, 'users/completed.html', {'message': message})


@login_required
def user_change_view(request):
    if request.method == 'POST':
        form = UserEditForm(request.POST, request.FILES, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('songs:feed')
        else:
            return render(request, 'users/edit.html', {'form': form})

    else:
        form = UserEditForm(instance=request.user)
        return render(request, 'users/edit.html', {'form': form})


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('users:edit')
        else:
            return render(request, 'users/change_password.html', {'form': form})

    else:
        form = PasswordChangeForm(user=request.user)
        return render(request, 'users/change_password.html', {'form': form})
