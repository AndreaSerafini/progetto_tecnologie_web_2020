from crispy_forms.layout import Submit
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms

from users.models import User
from crispy_forms.helper import FormHelper


class UserCrispyForm(UserCreationForm):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'username'
        self.fields['email'].widget.attrs['placeholder'] = 'example@example.com'
        self.fields['password1'].widget.attrs['placeholder'] = 'immetti la tua password qui'
        self.fields['password2'].widget.attrs['placeholder'] = 'ripeti la password'
        self.fields['first_name'].widget.attrs['placeholder'] = 'nome'
        self.fields['last_name'].widget.attrs['placeholder'] = 'cognome'
        self.fields['bio'].widget.attrs['placeholder'] = 'una breve descrizione di te...'
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Register', css_class='btn btn-success')
        )

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
            'bio',
            'avatar'
        )


class UserEditForm(UserChangeForm):
    avatar = forms.ImageField()

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'avatar',
        )
