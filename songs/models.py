from django.db import models
from users.models import User


## @file models.py
#  @package songs/model
#  Definizione del database per l'app "songs".
#
#  In questo modulo python vengono definite le tabelle e le relazioni relative \
#  all'applicazione songs
#


## Table: Genre
#  @class Genre
#
# Contains all musical genres created by admin.
class Genre(models.Model):

    ## @var name
    #  Name of musical genre
    name = models.CharField(max_length=30, unique=True,)

    ## @var description
    #  Description of the musical genre provided by admin
    description = models.TextField()

    ## @var favourite_genres
    #  Reifiication of the User-Genre relation
    #
    #  ManyToMany field (see django documentation at https://docs.djangoproject.com/en/4.0/topics/db/examples/many_to_many/) \
    #  between User and Genre. It tells if a certain <USER> likes a certain <GENRE>
    favourite_genres = models.ManyToManyField(User, related_name='favourite_genres', blank=True)

    ## Returns songs that belong to a certain <GENRE>
    #  @param self Instance of a musical genre
    #
    #  Given a certain instance of a genre, returns all songs in database that belong to this genre
    def get_songs(self):
        return list(self.songs.all())

    def __str__(self):
        return self.name


## Table: song
#  @class Song
#
#   Contains all the songs uploaded by users
class Song(models.Model):

    ## @var title
    #  Title of the song
    title = models.CharField(max_length=40, verbose_name="Titolo")

    ## @var audio_file
    #  Audio file related to the song
    #
    #  The audio file is stored in the folder indicated by "upload_to" parameter
    audio_file = models.FileField(upload_to='songs/',
                                  verbose_name='AudioFile')

    ## @var date
    #  The date the song was uploaded
    date = models.DateTimeField(auto_now_add=True)

    ## @var description
    #  Description of the song provided by user
    description = models.TextField(verbose_name='Descrizione')

    ## @var @post
    #  Post related to the song, provided by user
    post = models.TextField()

    ## @var genre
    #  Genre of the song
    #
    #  Foreign key to table Genre
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE, related_name='songs', verbose_name='Genere')

    ## @var composer
    #  Composer of the song
    #
    #  Foreign key to table User
    composer = models.ForeignKey('Composer', on_delete=models.CASCADE, related_name='songs')

    ## Returns the comments related to a song
    #  @param self Istance of a song
    #
    #  Given an instance of a song, returns the comments for that song sorted by the date of submission.
    def get_comments(self):
        return list(self.comments.all().order_by('-date_time'))

    ## Return the list of user that liked the song
    #  @param self Instance of a song
    def get_likes(self):
        return [like.user for like in self.likes.all()]


## Table: like
#  @class Like
#
#  Reification of the NtoN relation Users-Songs. Contains the likes instances between songs and users.
#  It defines the name of the relation in order to make the relation accessible from the tables song and user.
#  It also include the date of the like.
class Like(models.Model):

    ## @var song
    #  Foreign key totable song.
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name='likes')

    ## @var user
    #  Foreign key to table user.
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')

    ## @var date
    #  Date of submission of the like.
    date = models.DateField(auto_now_add=True)


## Table comment
#  @class Comment
#
#  It contains song's comments submitted by users.
class Comment(models.Model):

    ## @var text
    #  Text of the comment
    text = models.TextField()

    ## @var date_time
    #  Date and hour of submission of the comment
    date_time = models.DateTimeField(auto_now_add=True)

    ## @var song
    #  Foreign key to song's table.
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name='comments')

    ## @var user
    #  Foreign key to users' table
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')


## Table: composer
#  @class Composer
#
#  Contains users that are also composers.
#  An user can be either a simle user or a composer:
#      - user: only listens to composers' songs
#      - composer: can listen and upload songs
class Composer(models.Model):

    ## @var user
    #  OneToOne field to User table.
    #
    #  It specifies that the user is also a composer.
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='composers')

    ## @var art_name
    #  Art-name of the composer.
    art_name = models.CharField(max_length=30, unique=True, verbose_name="nome d'arte")

    ## @var favourite_composers
    #  Reification of the relation Users-Composers (user likes composers, a composer is liked by many users)
    favourite_composers = models.ManyToManyField(User, related_name='favourite_composers')

    ## @class: Composer.Meta
    #  Additional informations for class Composer
    class Meta:

        verbose_name = 'compositore'
        verbose_name_plural = 'compositori'

    ## Returns composer's full name
    #  @param self Composer's instance.
    def get_full_name(self):
        return self.user.get_full_name()

    ## Returns composer's list of followers
    #  @param self Composer's instance.
    def get_followers(self):
        return list(self.favourite_composers.all())

    ## Returns composer's profile picture.
    #  @param self Composer's instance.
    def get_avatar(self):
        return self.user.avatar
