from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from songs import views

app_name = 'songs'

urlpatterns = [
    path('feed/', views.feed, name='feed'),
    path('composer/<int:pk>/detail', views.ComposerDetailView.as_view(), name='composer-detail'),

    path('song/<int:pk>/detail', views.SongDetailView.as_view(), name='song-detail'),
    path('song/<int:pk>/modify', views.SongChangeView.as_view(), name='song-change'),
    path('song/<int:pk>/delete', views.SongDeleteView.as_view(), name='song-delete'),


    path('genre/<int:pk>/detail', views.GenreDetailView.as_view(), name='genre-detail'),

    path('insert_comment', views.insert_comment, name='insert-comment'),

    path('results', views.search, name='search'),
    path('get_genres', views.get_genres, name='get-genres'),
    path('get_songs', views.get_songs, name='get-songs'),
    path('get_composers', views.get_composers, name='get-composers'),

    path('following', views.following, name='following'),
    path('unfollow', views.unfollow, name='unfollow'),
    path('follow', views.follow, name='follow'),
    path('like', views.like, name='like'),


    path('become_composer', views.ComposerCreateView.as_view(), name='composer-create'),

    path('rankings', views.rankings, name="rankings"),
    path('dashboard', views.dashboard, name="dashboard"),
    path('refresh-graph', views.get_data_to_graph, name="refresh-data")
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
